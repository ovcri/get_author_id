"""
USAGE: call_serp [--serp-key=<key>]
"""

import docopt
import json
import csv
import sys
import os
from serpapi import GoogleScholarSearch

def main():
    args = docopt.docopt(__doc__)
    if args["--serp-key"]:
        key = args["--serp-key"]
    else:
        try:
            key = os.environ["SERP_KEY"]
        except KeyError:
            print("SERP_KEY environment variable not set. Either set the environment variable, or pass in the key as a commandline argument", file=sys.stderr)
            exit(1)

    writer = csv.writer(sys.stdout)
    reader = csv.reader(sys.stdin)
    next(reader)

    lastnames = set()
    for line in reader:
        ttid = line[0]
        author = line[1]
        
        lastname, firstname = author.split(",")

        
        #first search full name
        query = GoogleScholarSearch({
            "engine": "google_scholar_profiles",
            "mauthors":author,
            "api_key": key,
            })
        
        data = query.get_dict()
        
        # if we can't find it based on fullname, try search by last name
        if "profiles" not in data:
            query = GoogleScholarSearch({
                "engine": "google_scholar_profiles",
                "mauthors":lastname,
                "api_key": key,
                })
            data = query.get_dict()

        # If we still dont have data for this author, give up
        if "profiles" not in data:
            writer.writerow([ttid, author, ""])
            continue
        else:
            profiles = data["profiles"]

        # Now we search for the authors that are affiliated with S&T
        for profile in profiles:
            if "mst.edu" in profile.get("email",""):
                # OK found it :)
                writer.writerow([ttid, author, profile.get("author_id", "")])
                lastnames.add(lastname)
                break
        else:
            # Nope :(
            writer.writerow([ttid, author, ""])

if __name__ == "__main__":
    main()
