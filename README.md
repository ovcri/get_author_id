# get_author_id

Get the google scholar ID of S&T faculty

## Dependencies
The scripts depend on two packages: GoogleSearch and docopt. To install:

```
pip3 install docopt
pip3 install google-search-results
```

Also required is the api key from https://serpapi.com

## Input Data Format

_call_serp.py_ reads a csv file from standard input. The CSV file is structured like so:

| TT_ID | NAME |
| ---- | ------ |
| 001 | LastName,FirstName |


**_call_serp.py_ assumes that there's a header, and so the first line is stripped. The ID field is not used.**

## Output Data Fromat

The output of _call_serp.py_ should be piped to _strip_https.py_. Otherwise, there will be some extra lines starting with 'https'

| TT_ID | NAME | AUTHOR_ID |
| ------ | ------ | ---- |
| 001 | LastName,FirstName | xxxxxx1|
 

## Usage

There are two scripts in this repo: "call_serp.py" and "strip_https.py".

The API key may be provided via environment variable
```
export SERP_KEY="YOUR API KEY HERE"
cat "$INPUT_FILE" | python3 call_serp.py | python3 strip_https.py > "$OUTPUT_FILE"
```

Or the API key may be provided through command line argument
```
cat "$INPUT_FILE" | python3 call_serp.py --serp-key="$SERP_KEY" | python3 strip_https.py > "$OUTPUT_FILE"
```




